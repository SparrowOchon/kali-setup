# CTF-setup

Kalibox setup for CTF's.

## Usage

1. Run the `setup.sh` file

## Whats inside:

1. Stego tools
2. Crypto tools
3. Reverse Engineering tools
4. Mobile tools
5. Web tools
6. Forensics tools
7. Bash aliases to make your life easier
8. Proper vimrc
9. Proper tmux config
