#!/bin/bash
#License:
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
# By: Network Silence (23-June-2017)
#
# Script to setup kali for CTFs
#
#
install_dir="/usr/share"
PASS=''
if [[ $(/usr/bin/id -u) -eq 0 ]]; then
    echo "Do not run the following Script as Root"
    exit
fi
function get_sudoer_pass(){
	while [[ $PASS == "" ]]; do
		sudo -k
		echo -n "Enter the sudo password:"
		read -r temppass
		echo "$temppass" | sudo -S echo testpass &> /dev/null
		if ! [ "$(sudo -n echo testpass 2>&1)" == "testpass" ]; then
			echo "Incorrect password was entered"
		else
			PASS=$temppass
		fi
	done
}
function stego_setup(){
	git clone https://github.com/zardus/ctf-tools.git
	echo "$PASS" | sudo -S ctf-tools/stegsolve/install
	echo "$PASS" | sudo -S ctf-tools/stegdetect/install-root-debian
	echo "$PASS" | sudo -S ctf-tools/stegdetect/install
	echo "$PASS" | sudo -S ctf-tools/steganabara/install

	gem install zsteg

	echo "$PASS" | sudo -S curl https://raw.githubusercontent.com/evyatarmeged/stegextract/master/stegextract > /usr/local/bin/stegextract
	echo "$PASS" | sudo -S chmod +x /usr/local/bin/stegextract

	# Dir Enumeration Tool
	go get github.com/OJ/gobuster

}
function crypto_tools(){
	git clone https://github.com/nccgroup/featherduster.git # Crypto Analyzer
	python3 featherduster/setup.py install


	# Password List for cracking
	curl https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10-million-password-list-top-10000.txt > /usr/share/wordlist/darkweb2017-top10000.txt


	git clone https://github.com/lanjelot/patator.git # Brute Forcer
	echo "$PASS" | sudo -S pip3 install -r patator/requirements.txt
	ln -s $install_dir/patator/patator.py /usr/bin/ftp_login
	ln -s $install_dir/patator/patator.py /usr/bin/ssh_login
	ln -s $install_dir/patator/patator.py /usr/bin/telnet_login
	ln -s $install_dir/patator/patator.py /usr/bin/smtp_login
	ln -s $install_dir/patator/patator.py /usr/bin/smtp_vrfy
	ln -s $install_dir/patator/patator.py /usr/bin/smtp_rcpt
	ln -s $install_dir/patator/patator.py /usr/bin/finger_lookup
	ln -s $install_dir/patator/patator.py /usr/bin/http_fuzz
	ln -s $install_dir/patator/patator.py /usr/bin/rdp_gateway
	ln -s $install_dir/patator/patator.py /usr/bin/ajp_fuzz
	ln -s $install_dir/patator/patator.py /usr/bin/pop_login
	ln -s $install_dir/patator/patator.py /usr/bin/pop_passd
	ln -s $install_dir/patator/patator.py /usr/bin/imap_login
	ln -s $install_dir/patator/patator.py /usr/bin/ldap_login
	ln -s $install_dir/patator/patator.py /usr/bin/smb_login
	ln -s $install_dir/patator/patator.py /usr/bin/smb_lookupsid
	ln -s $install_dir/patator/patator.py /usr/bin/rlogin_login
	ln -s $install_dir/patator/patator.py /usr/bin/vmauthd_login
	ln -s $install_dir/patator/patator.py /usr/bin/mssql_login
	ln -s $install_dir/patator/patator.py /usr/bin/oracle_login
	ln -s $install_dir/patator/patator.py /usr/bin/mysql_login
	ln -s $install_dir/patator/patator.py /usr/bin/mysql_query
	ln -s $install_dir/patator/patator.py /usr/bin/rdp_login
	ln -s $install_dir/patator/patator.py /usr/bin/pgsql_login
	ln -s $install_dir/patator/patator.py /usr/bin/vnc_login
	ln -s $install_dir/patator/patator.py /usr/bin/dns_forward
	ln -s $install_dir/patator/patator.py /usr/bin/dns_reverse
	ln -s $install_dir/patator/patator.py /usr/bin/snmp_login
	ln -s $install_dir/patator/patator.py /usr/bin/ike_enum
	ln -s $install_dir/patator/patator.py /usr/bin/unzip_pass
	ln -s $install_dir/patator/patator.py /usr/bin/keystore_pass
	ln -s $install_dir/patator/patator.py /usr/bin/sqlcipher_pass
	ln -s $install_dir/patator/patator.py /usr/bin/umbraco_crack
	ln -s $install_dir/patator/patator.py /usr/bin/tcp_fuzz
}
function forensics_tools(){
	echo "$PASS" | sudo -S pip3 install fibratus # Capture windows kernel activity
}
function reverse_engineering_tools(){
	echo "$PASS" | sudo -S pip3 install angr # Binary Analysis Framework

	echo "$PASS" | sudo -S pip3 install frida-tools # GreaseMonkey for native apps

	opam install cwe_checker # Common Weakness checker

	python3 -m pip install --upgrade git+https://github.com/Gallopsled/pwntools.git@dev3 # Pwntools (python3)

	git clone https://github.com/pwndbg/pwndbg # make GDB reverse friendly
	echo "$PASS" | sudo -S pwndbg/setup.sh
 }
function mobile_tools(){
	pip3 install objection # Best mobile exploitation framework
}
function config_setup(){
	mv configs/bash_alias /root/.bash_aliases
	mv configs/vim /root/.vimrc
	mv configs/tmux /root/.tmux.conf
	mv configs/valgrind /root/.valgrindrc
	source /root/.bash_aliases
	tmux source-file /root/.tmux.conf
}
function main(){
	get_sudoer_pass
	echo "$PASS" | sudo -S apt-get update -y
	echo "$PASS" | sudo -S apt-get upgrade -y
	echo "$PASS" | sudo -S apt-get install grc golang tmux valgrind python3 \
		libncurses-dev python3-pip ruby opam git libssl-dev libffi-dev \
		build-essential default-jre libldap-2.4-2 unzip freerdp2-x11 \
		libsqlite3-dev libsqlcipher-dev ike-scan freerdp2-x11 curl -y
	cd $install_dir || exit
	config_setup
	python3 -m pip install --upgrade pip

	stego_tools
	crypto_tools
	reverse_engineering_tools
	forensics_tools
	mobile_tools
}
main "$@"
